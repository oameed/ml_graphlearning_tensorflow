###################################################
### GRAPH LEARNING WITH TENSORFLOW 2            ###
### CONVOLUTIONAL GRAPH NEURAL NETWORKS (GNNs)  ###
### TRAINING CONVOLUTIONAL GNN NODDE CLASSIFIER ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import                        os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import                        sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import                        numpy            as np
import                        tensorflow       as tf
from paramd           import  PATHS, PARAMS
from inputd           import (data_get_graph_info    ,
                              data_inputs_process    ,
                              saveTFRECORDS          ,
                              inputTFRECORDS          )
from utilsd           import (load_processed_data    ,
                              plotter                ,
                              wHDF                   ,
                              plot_to_image           )
from nndGNNClassifier import (callback_custom_ckpt   ,
                              callback_custom_monitor,
                              callback_custom_history, 
                              create_ffn             ,
                              GraphConvLayer         ,
                              GNNNodeClassifier       )

tfr_train        =os.path.join(PATHS[2],'train'                      )
tfr_validation   =os.path.join(PATHS[2],'validation'                 )
log_train        =os.path.join(PATHS[4],'train'                      )
data_monitor_name=os.path.join(PATHS[0],'processed','processed'+'.h5')

graph_info       =data_get_graph_info(PATHS)
data_inputs_process                  (PATHS)

#############
### MODEL ###
#############
model     =GNNNodeClassifier(graph_info    =graph_info       ,
                             num_classes   =len(PARAMS[2]   ),
                             hidden_units  =    PARAMS[1][0] ,
                             create_ffn    =create_ffn       ,
                             GraphConvLayer=GraphConvLayer   ,
                             paths         =PATHS            ,
                             params        =PARAMS           ,
                             dropout_rate  =    PARAMS[1][1] ,
                             name          ="gnn_model"       )

model.compile               (optimizer     = tf.keras.optimizers.Adam                     (PARAMS[1][2]    ) ,
                             loss          = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True) ,
                             metrics       =[tf.keras.metrics.SparseCategoricalAccuracy   (name="acc"      )] )

#################
### CALLBACKS ###
#################
callbacks_train=[callback_custom_ckpt            ()                                                                  ,
                 callback_custom_monitor         (writer              =tf.summary.create_file_writer(log_train)     ,
                                                  data                =load_processed_data(data_monitor_name,'test'),
                                                  plotter             =plotter                                      ,
                                                  converter           =plot_to_image                                 ),
                 callback_custom_history         (plotter             =plotter                                      ,
                                                  whdf                =wHDF                                          ),
                 tf.keras.callbacks.TensorBoard  (log_dir             =log_train                                    , 
                                                  histogram_freq      =1                                            , 
                                                  update_freq         ='batch'                                       ),
                 tf.keras.callbacks.EarlyStopping(monitor             ="val_acc"                                    , 
                                                  patience            =50                                           , 
                                                  restore_best_weights=True                                          ) ]

#############
### TRAIN ###
#############
print        (' WRITING  TRAINING   DATA TO TFRECORDS FORMAT ')
saveTFRECORDS(PATHS,PARAMS,'train')

print        (' FITTING  MODEL'                     )
data_train     =inputTFRECORDS(tfr_train     ,PARAMS)
data_validation=inputTFRECORDS(tfr_validation,PARAMS)

model.fit(x                    =data_train     ,
          epochs               =PARAMS[0][2]   ,
          validation_data      =data_validation,
          callbacks            =callbacks_train,
          verbose              =1               )
print    (' TRAINING FINISHED '                 )


