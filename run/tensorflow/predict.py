####################################################
### GRAPH LEARNING WITH TENSORFLOW 2             ###
### CONVOLUTIONAL GRAPH NEURAL NETWORKS (GNNs)   ###
### GENERATING PREDICTIONS USING A TRAINED MODEL ###
### by: OAMEED NOAKOASTEEN                       ###
####################################################

import                              os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import                              sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import         numpy             as np
import         tensorflow        as tf
from   matplotlib import  pyplot as  plt
from paramd       import  PATHS, PARAMS
from utilsd       import (load_processed_data,
                          plotter             )

datafilename =os.path.join              (PATHS[0]     ,'processed'  ,'processed'+'.h5')
savefilename =os.path.join              (PATHS[5]     ,'predictions'                  )
x,Y          =load_processed_data       (datafilename ,'test'                         )

model        =tf.keras.models.load_model(PATHS[3])

y            =model                    (x)
y            =tf.keras.layers.Softmax()(y)
y            =np.array                 ([np.argmax(i) for i in y])
fig          =plotter                  ('cm', [Y,y], [PARAMS[2]]      )
plt.savefig                            (savefilename+'.png',format='png')


