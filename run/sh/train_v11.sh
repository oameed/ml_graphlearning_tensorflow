#! /bin/bash

source  activate tf2panda
cd      run/tensorflow

echo ' CREATING PROJECT DIRECTORY '                            'v11'
rm     -rf                                       ../../networks/v11
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v11

echo ' TRAINING GNN CLASSIFIER ON CORA '
python train_gnn_classifier.py -net v11 -b 128 -epc 100

echo ' GENERATING PREDICTIONS '
python predict.py              -net v11


