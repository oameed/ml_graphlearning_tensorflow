#! /bin/bash

#SBATCH --ntasks=8
#SBATCH --time=48:00:00
#SBATCH --partition=singleGPU
#SBATCH --gres=gpu:1
#SBATCH --mail-type=BEGIN,FALI,END
#SBATCH --mail-user=
#SBATCH --job-name=GNN-Classifier

module load anaconda3

source  activate tf2panda
cd      $SLURM_SUBMIT_DIR
cd      ../tensorflow


echo ' CREATING PROJECT DIRECTORY '                            'v11'
rm     -rf                                       ../../networks/v11
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v11

echo ' TRAINING GNN CLASSIFIER ON CORA '
python train_gnn_classifier.py -net v11 -b 128 -epc 100

echo ' GENERATING PREDICTIONS '
python predict.py              -net v11


