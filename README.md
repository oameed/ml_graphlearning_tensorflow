# Graph Learning with [TensorFlow](https://www.tensorflow.org/)

## References

_General_

[[G 1 ].](https://github.com/tensorflow/gnn/blob/main/tensorflow_gnn/docs/guide/intro.md) 2021. TensorFlow. _Introduction to Graph Neural Networks_ [\[TensorFlow on YouTube: ML Tech Talks\]](https://www.youtube.com/watch?v=8owQBFAHw7E)  
[[G 2 ].](https://distill.pub/2021/gnn-intro/) 2021. Sanchez-Lengeling. _A Gentle Introduction to Graph Neural Networks_  
[[G 3 ].](https://www.cambridge.org/core/books/deep-learning-on-graphs/CF908050EECC148A9E6F3EAED6099DB4) 2021. Ma. _Deep Learning on Graphs_ [\[view online\]](https://web.njit.edu/~ym329/dlg_book/)  
[[G 4 ].](https://www.morganclaypoolpublishers.com/catalog_Orig/product_info.php?products_id=1576) 2020. Hamilton. _Graph Representation Learning_ [\[view online\]](https://www.cs.mcgill.ca/~wlh/grl_book/)  
[[G 5 ].](https://arxiv.org/abs/2010.05234) 2020. Ward. _A Practical Tutorial on Graph Neural Networks_ [\[GitHub\]](https://github.com/isolabs/gnn-tutorial)  
[[G 6 ].](https://arxiv.org/abs/1812.08434) 2018. Zhou. _Graph Neural Networks: A Review of Methods and Applications_ [\[GitHub\]](https://github.com/thunlp/GNNPapers)  

_Architecture_

[[A 1 ].](https://arxiv.org/abs/1609.02907) 2016. Kipf. _Semi-Supervised Classification with Graph Convolutional Networks_ [\[Github\]](https://github.com/tkipf/gcn) [\[Blog\]](https://tkipf.github.io/graph-convolutional-networks/)  

_Useful Tutorials_

[[U 1 ].](http://web.stanford.edu/class/cs224w/) 2021. Stanford CS224W. _Machine Learning with Graphs_  
[[U 2 ].](https://uvadlc-notebooks.readthedocs.io/en/latest/tutorial_notebooks/tutorial7/GNN_overview.html) UvA. _Graph Neural Networks_  

_Other Frameworks_

[[F 1 ].](https://blog.tensorflow.org/2021/11/introducing-tensorflow-gnn.html) _Introducing TensorFlow Graph Neural Networks_ [\[GitHub\]](https://github.com/tensorflow/gnn)  
[[F 2 ].](https://graphneural.network/) _Spektral_  
[[F 3 ].](https://github.com/deepmind/graph_nets) _Deep Mind's Graph Nets_  

## Acknowledgements

* We would like to thank the University of New Mexico Center for Advanced Research Computing [(CARC)](http://carc.unm.edu/), supported in part by the National Science Foundation, for providing the high performance computing resources used in this work.

* The GNN Node Classifier code used in this project is an adaptation from Keras Code Examples: [_Node Classification with Graph Neural Networks_](https://keras.io/examples/graph/gnn_citations/).  

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.82  T=0.02 s (636.0 files/s, 56391.9 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                           7             79             78            518
YAML                             2              1              0            257
Markdown                         1             25              0             60
Bourne Shell                     2             15              7             24
-------------------------------------------------------------------------------
SUM:                            12            120             85            859
-------------------------------------------------------------------------------
</pre>

## How to Run

* This project uses `Tensorflow` Version `2.2` along with `cudatoolkit` Version `10.1.243` and `cudnn` Version `7.6.5`. The `YAML` files for creating the conda environments used to run this project are included in `run/conda`  

* To run experiments (with training a GNN classifier as an example):
  * on Local PC:
    1. `cd` to main project directory
    2. `./run/sh/train_v11.sh`
  * on HPC system:
    1. `cd` to main project directory
    2. `cd ./run/slrm/`
    3. `sbatch train_v11.sh`

## Dataset

|     |
|:---:|
**[Cora](https://relational.fit.cvut.cz/dataset/CORA)**|
![][fig_1]  |

[fig_1]:data/cora/samples/samples.png

## Experiments

### v11: GNN Node Classifier

|     |     |     |
|:---:|:---:|:---:|
![][v11_1] | ![][v11_2] | ![][v11_3]

[v11_1]:networks/v11/logs/train/loss.png
[v11_2]:networks/v11/logs/train/accuracy.png
[v11_3]:networks/v11/predictions/predictions.png


