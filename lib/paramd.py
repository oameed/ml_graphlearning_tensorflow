##################################################
### GRAPH LEARNING WITH TENSORFLOW 2           ###
### CONVOLUTIONAL GRAPH NEURAL NETWORKS (GNNs) ###
### PARAMETER DEFINITIONS                      ###
### by: OAMEED NOAKOASTEEN                     ###
##################################################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-net' , type=str  , default='v00', help='NAME-OF-NETWORK-PROJECT'                         )
  # GROUP: HYPER-PARAMETERS FOR TRAINING
parser.add_argument('-b'   , type=int  , default=128  , help='BATCH-SIZE'                                      )
parser.add_argument('-epc' , type=int  , default=50   , help='NUMBER-OF-TRAINING-EPOCHS'                       )
parser.add_argument('-bc'  , type=int  , default=10000, help='BATCH-BUFFER-CAPACITY'                           )
parser.add_argument('-vs'  , type=float, default=0.1  , help='VALIDATION SPLIT '                               )
 # GROUP:  HYPER-PARAMETERS FOR ARCHITECTURES

### ENABLE FLAGS
args=parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES

PATHS=[os.path.join('..','..','data'    ,'cora'                 ),
       os.path.join('..','..','data'    ,'cora'  ,'samples'     ),
       os.path.join('..','..','networks',args.net,'tfrecords'   ),
       os.path.join('..','..','networks',args.net,'checkpoints' ),
       os.path.join('..','..','networks',args.net,'logs'        ),
       os.path.join('..','..','networks',args.net,'predictions' ) ] 

PARAMS   =   [[args.net                ,
               args.b                  ,
               args.epc                ,
               args.bc                 ,
               args.vs                  ]]

PARAMS.append([[32  ,32],
                0.5     ,
                0.01     ])

PARAMS.append(['Case_Based'            ,
               'Genetic_Algorithms'    ,
               'Neural_Networks'       ,
               'Probabilistic_Methods' ,
               'Reinforcement_Learning',
               'Rule_Learning'         ,
               'Theory'                 ])


# PARAMS[0][0] : NETWORK DIRECTORY
# PARAMS[0][1] : BATCH SIZE
# PARAMS[0][2] : TRAINING EPOCHS
# PARAMS[0][3] : BATCH BUFFER CAPACITY
# PARAMS[0][4] : VALIDATION SPLIT RATIO
# PARAMS[1][0] : HIDDEN UNITS
# PARAMS[1][1] : DROPOUT RATE
# PARAMS[1][2] : LEARNING RATE
# PARAMS[2][*] : DATASET'S CLASSES


