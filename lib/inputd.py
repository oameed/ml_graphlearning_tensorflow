##################################################
### GRAPH LEARNING WITH TENSORFLOW 2           ###
### CONVOLUTIONAL GRAPH NEURAL NETWORKS (GNNs) ###
### INPUT FUNCTION DEFINITIONS                 ###
### by: OAMEED NOAKOASTEEN                     ###
##################################################

import                os
import                numpy            as np
import                tensorflow       as tf
from datasetd import  dataset_get_cora
from utilsd   import (getFILENAMES       ,
                      wHDF               ,
                      load_processed_data )

def data_get_graph_info(PATHS,VISUALIZE=False):
 papers,citations=dataset_get_cora(PATHS[0],VISUALIZE)
 feature_names   =set(papers.columns)-{"paper_id","subject"}
 node_features   =tf.cast(papers.sort_values("paper_id")[feature_names].to_numpy(), dtype=tf.dtypes.float32)
 edges           =citations[["source", "target"]].to_numpy().T
 edge_weights    =tf.ones(shape=edges.shape[1])
 return (node_features,edges,edge_weights)

def data_get_inputs(PATHS):
 import pandas as pd
 split     =0.5
 papers, _ =dataset_get_cora(PATHS[0])
 data_train=[]
 data_test =[]
 for _, group_data in papers.groupby("subject"):
  size            =len(group_data.index)
  indeces         =np.random.choice(size,int(split*size),replace=False)
  random_selection=np.array([i in indeces for i in range(size)])
  data_train.append(group_data[ random_selection])
  data_test.append (group_data[~random_selection])
 data_train=pd.concat(data_train).sample(frac=1)
 data_test =pd.concat(data_test ).sample(frac=1)
 x_train   =data_train["paper_id"].to_numpy()
 x_test    =data_test ["paper_id"].to_numpy()
 y_train   =data_train["subject" ].to_numpy()
 y_test    =data_test ["subject" ].to_numpy()
 return x_train, y_train, x_test, y_test


def data_inputs_process(PATHS):
 x_train,y_train,x_test,y_test=data_get_inputs(PATHS)
 savefilename                 =os.path.join(PATHS[0],'processed','processed'+'.h5')
 wHDF(savefilename,
      ['x_train','y_train','x_test','y_test'],
      [ x_train , y_train , x_test , y_test ] )

def saveTFRECORDS(PATHS, PARAMS, MODE):
 def get_train_validation_splits(X,Y,SPLIT):
  indeces           =[i for i in range(X.shape[0])]
  indeces_train     =np.random.choice(indeces,int((1-SPLIT)*X.shape[0]), replace=False).tolist()
  indeces_validation=[i for i in indeces if not i in indeces_train]
  x                 =X.tolist()
  y                 =Y.tolist()
  x_train           =np.array([x[i] for i in indeces_train     ])
  y_train           =np.array([y[i] for i in indeces_train     ])
  x_validation      =np.array([x[i] for i in indeces_validation])
  y_validation      =np.array([y[i] for i in indeces_validation])
  return x_train, y_train, x_validation, y_validation
 def serialize_example(X,Y):
  feature      ={'x':tf.train.Feature(int64_list=tf.train.Int64List(value=[X])), 
                 'y':tf.train.Feature(int64_list=tf.train.Int64List(value=[Y])) }
  example_proto=tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()
 def write_serialized_example(X,Y,MODE):
  filename =os.path.join(PATHS[2],MODE,'batch'+'_'+str(1)+'.tfrecords')
  with tf.io.TFRecordWriter(filename) as writer:
   for i in range(X.shape[0]):
    example=serialize_example(X[i],Y[i])
    writer.write(example)
 readfilename   =os.path.join(PATHS[0],'processed','processed'+'.h5')
 if  MODE in ['train']:
  x_train,y_train                          =load_processed_data        (readfilename,'train'        )
  x_train,y_train,x_validation,y_validation=get_train_validation_splits(x_train,y_train,PARAMS[0][4])
  write_serialized_example (x_train     , y_train     , MODE        )
  write_serialized_example (x_validation, y_validation, 'validation')
 else:
  if MODE in ['test' ]:
   x_test ,y_test                          =load_processed_data        (readfilename,'test'         )
   write_serialized_example(x_test      , y_test      , MODE        )
 
def inputTFRECORDS(PATH, PARAMS):
 filenames=getFILENAMES(PATH)
 feature  ={'x':tf.io.FixedLenFeature([],tf.int64), 
            'y':tf.io.FixedLenFeature([],tf.int64) }
 def parse_function(example_proto):
  parsed_example=tf.io.parse_single_example(example_proto,feature )
  x             =parsed_example['x']
  y             =parsed_example['y']
  return x,y
 dataset  =tf.data.TFRecordDataset(filenames                                    )
 dataset  =dataset.map            (parse_function                               )
 dataset =dataset.batch           (PARAMS[0][1]  , drop_remainder          =True)
 dataset =dataset.shuffle         (PARAMS[0][3]  , reshuffle_each_iteration=True)
 return dataset


