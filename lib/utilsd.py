##################################################
### GRAPH LEARNING WITH TENSORFLOW 2           ###
### CONVOLUTIONAL GRAPH NEURAL NETWORKS (GNNs) ###
### UTILITY FUNCTION DEFINITIONS               ###
### by: OAMEED NOAKOASTEEN                     ###
##################################################

def getFILENAMES(PATH,COMPLETE=True):
 import os
 fn=[]
 for FILE in os.listdir(PATH):
  if COMPLETE:
   if not FILE.startswith('.'):
    fn.append(os.path.join(PATH,FILE))
  else:
   if not FILE.startswith('.'):
    fn.append(FILE)
 return fn

def wHDF(FILENAME,DIRNAMES,DATA):
 import h5py
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()

def rHDF(FILENAME):
 import h5py
 fobj=h5py.File(FILENAME,'r')
 keys=[key for key in fobj.keys()]
 return fobj,keys

def load_processed_data(FILENAME,MODE):
 fobj,keys=rHDF(FILENAME)
 if  MODE in ['train']:
  x=fobj['x_train'][:]
  y=fobj['y_train'][:]
 else:
  if MODE in ['test' ]:
   x=fobj['x_test'][:]
   y=fobj['y_test'][:]
 return x,y

def plotter(MODE,DATA,CONFIG):
 import numpy                  as np
 import tensorflow             as tf
 from matplotlib import pyplot as plt
 if       MODE=='cm'     :
  import itertools
  # CONFIG[0]: CLASS NAMES
  cm =tf.math.confusion_matrix(DATA[0],DATA[1],dtype=tf.float32).numpy()
  fig=plt.figure(figsize=(8, 8))
  plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
  plt.title("Confusion Matrix")
  plt.colorbar()
  plt.xticks(np.arange(len(CONFIG[0])), CONFIG[0], rotation=45)
  plt.yticks(np.arange(len(CONFIG[0])), CONFIG[0]             )
  labels   =np.around(cm/cm.sum(axis=1)[:, np.newaxis], decimals=2)
  threshold=cm.max()/2.0
  for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    color  = "white" if cm[i, j] > threshold else "black"
    plt.text(j, i, labels[i, j], horizontalalignment="center", color=color)
  plt.tight_layout()
  plt.ylabel('True Label')
  plt.xlabel('Predicted Label')
 else:
  if      MODE=='metrics':
   # CONFIG[0]: COLOR CODES
   idx=np.array([i/1e3 for i in range(len(DATA[1][0]))])
   fig,ax=plt.subplots()
   for i in range(len(DATA[0])):
    ax.plot(idx, DATA[1][i], CONFIG[0][i], linewidth=2, label=DATA[0][i])
   ax.set_title (' Training Metrics ')
   ax.set_xlabel(' Iterations (K) '  )
   ax.legend    ()
   ax.grid      ()
 return fig

def plot_to_image(figure):
 import                               io
 import                 tensorflow as tf
 from matplotlib import pyplot     as plt
 buf  =io.BytesIO()
 plt.savefig(buf, format='png')
 plt.close  (figure) 
 buf.seek   (0)
 image=tf.image.decode_png(buf.getvalue(), channels=0)
 image=tf.expand_dims     (image         ,          0)
 return image


