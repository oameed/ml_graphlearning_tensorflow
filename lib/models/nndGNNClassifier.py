##################################################
### GRAPH LEARNING WITH TENSORFLOW 2           ###
### CONVOLUTIONAL GRAPH NEURAL NETWORKS (GNNs) ###
### TRAINING CONVOLUTIONAL GNN NODE CLASSIFIER ###
### LAYERS, MODEL & CALLBACK DEFINITIONS       ###
### by: OAMEED NOAKOASTEEN                     ###
##################################################

import os
import numpy                  as  np
import tensorflow             as  tf
from matplotlib import pyplot as plt

##############
### LAYERS ###
##############

def create_ffn(hidden_units, dropout_rate, name=None):
 fnn_layers = []
 for units in hidden_units:
  fnn_layers.append(tf.keras.layers.BatchNormalization()                            )
  fnn_layers.append(tf.keras.layers.Dropout           (dropout_rate                ))
  fnn_layers.append(tf.keras.layers.Dense             (units, activation=tf.nn.relu))
 return tf.keras.Sequential(fnn_layers, name=name)

#############
### MODEL ###
#############

class GraphConvLayer(tf.keras.layers.Layer):
    
    def __init__(self                     , 
                 hidden_units             ,
                 create_ffn               ,
                 dropout_rate    =0.2     ,
                 aggregation_type="mean"  ,
                 combination_type="concat",
                 normalize       =False   ,
                 *args                    ,
                 **kwargs                 ,
                                           ):
     super().__init__()
     self.create_ffn       =create_ffn
     self.aggregation_type =aggregation_type
     self.combination_type =combination_type
     self.normalize        =normalize
     self.ffn_prepare      =self.create_ffn(hidden_units, dropout_rate)
     if self.combination_type == "gated":
      self.update_fn       =tf.keras.layers.GRU(units               =hidden_units,
                                                activation          ="tanh"      ,
                                                recurrent_activation="sigmoid"   ,
                                                dropout             =dropout_rate,
                                                return_state        =True        ,
                                                recurrent_dropout   =dropout_rate )
     else:
      self.update_fn       =self.create_ffn(hidden_units, dropout_rate)
    
    def prepare(self, node_repesentations, weights=None):
     messages =self.ffn_prepare(node_repesentations)
     if weights is not None:
      messages=messages*tf.expand_dims(weights, -1)
     return messages

    def aggregate(self, node_indices, neighbour_messages):
     num_nodes=tf.math.reduce_max(node_indices) + 1
     if   self.aggregation_type=="sum" :
      aggregated_message  =tf.math.unsorted_segment_sum (neighbour_messages, node_indices, num_segments=num_nodes)
     else:
      if  self.aggregation_type=="mean":
       aggregated_message =tf.math.unsorted_segment_mean(neighbour_messages, node_indices, num_segments=num_nodes)
      else:
       if self.aggregation_type=="max" :
        aggregated_message=tf.math.unsorted_segment_max (neighbour_messages, node_indices, num_segments=num_nodes)
       else:
        raise ValueError(f"Invalid aggregation type: {self.aggregation_type}.")
     return aggregated_message

    def update(self, node_repesentations, aggregated_messages):
     if   self.combination_type=="gru"   :
      h   =tf.stack ([node_repesentations, aggregated_messages], axis=1)
     else:
      if  self.combination_type=="concat":
       h  =tf.concat([node_repesentations, aggregated_messages], axis=1)
      else:
       if self.combination_type=="add"   :
         h=node_repesentations + aggregated_messages
       else:
        raise ValueError(f"Invalid combination type: {self.combination_type}.")
     node_embeddings =self.update_fn(h)
     if self.combination_type == "gru":
      node_embeddings=tf.unstack        (node_embeddings, axis= 1)[-1]
     if self.normalize:
      node_embeddings=tf.nn.l2_normalize(node_embeddings, axis=-1)
     return node_embeddings

    def call(self, inputs):
     node_repesentations, edges, edge_weights=inputs
     node_indices                            =edges[0] 
     neighbour_indices                       =edges[1]
     neighbour_repesentations                =tf.gather     (node_repesentations     , neighbour_indices )
     neighbour_messages                      =self.prepare  (neighbour_repesentations, edge_weights      )
     aggregated_messages                     =self.aggregate(node_indices            , neighbour_messages)
     return self.update(node_repesentations, aggregated_messages)

class GNNNodeClassifier(tf.keras.Model):
    
    def __init__(self                     ,
                 graph_info               ,
                 num_classes              ,
                 hidden_units             ,
                 create_ffn               ,
                 GraphConvLayer           ,
                 paths                    ,
                 params                   ,
                 aggregation_type="sum"   ,
                 combination_type="concat",
                 dropout_rate    =0.2     ,
                 normalize       =True    ,
                 *args                    ,
                 **kwargs                  ):
     super().__init__()
     self.create_ffn    =create_ffn
     self.GraphConvLayer=GraphConvLayer
     self.node_features =graph_info[0]
     self.edges         =graph_info[1]
     self.edge_weights  =graph_info[2]
     if self.edge_weights is None:
      self.edge_weights =tf.ones(shape=edges.shape[1])
     self.edge_weights  =self.edge_weights/tf.math.reduce_sum(self.edge_weights)
     self.preprocess    =self.create_ffn      (hidden_units      ,  
                                               dropout_rate      , 
                                               name="preprocess"  )
     self.conv1         =self.GraphConvLayer  (hidden_units      ,
                                               self.create_ffn   ,
                                               dropout_rate      ,
                                               aggregation_type  ,
                                               combination_type  ,
                                               normalize         ,
                                               name="graph_conv1" )
     self.conv2         =self.GraphConvLayer  (hidden_units      ,
                                               self.create_ffn   ,
                                               dropout_rate      ,
                                               aggregation_type  ,
                                               combination_type  ,
                                               normalize         ,
                                               name="graph_conv2" )
     self.postprocess   =self.create_ffn      (hidden_units      , 
                                               dropout_rate      , 
                                               name="postprocess" )
     self.compute_logits=tf.keras.layers.Dense(units=num_classes , 
                                               name ="logits"     )
     self.paths         =paths
     self.params        =params

    def call(self, input_node_indices):
     x              =self.preprocess    ( self.node_features               )
     x1             =self.conv1         ((x, self.edges, self.edge_weights))
     x              =x1 + x
     x2             =self.conv2         ((x, self.edges, self.edge_weights))
     x              =x2 + x
     x              =self.postprocess   ( x                                )
     node_embeddings=tf.gather          ( x, input_node_indices            )
     logits         =self.compute_logits( node_embeddings                  )
     return logits
    
    def train_step(self,data):
     x,y=data
     with tf.GradientTape() as tape:
      predictions=self(x, training=True)
      loss       =self.compiled_loss(y, predictions, regularization_losses=self.losses)
     gradients   =tape.gradient        (loss         , self.trainable_weights )
     self.optimizer.apply_gradients    (zip(gradients, self.trainable_weights))
     self.compiled_metrics.update_state(    y        , predictions            )
     return {"loss"    :loss                                           ,
             "accuracy":self.compiled_metrics._user_metrics[0].result() }

##########################
### TRAINING CALLBACKS ###
##########################

class callback_custom_ckpt(tf.keras.callbacks.Callback):

    def on_epoch_end(self, epoch, logs=None):
        tf.keras.models.save_model(self.model ,self.model.paths[3])


class callback_custom_monitor(tf.keras.callbacks.Callback):

    def __init__(self, writer, data, plotter, converter):
        super().__init__()
        self.writer   =writer
        self.data     =data
        self.plotter  =plotter
        self.converter=converter

    def on_epoch_begin(self, epoch, logs=None):
        x,Y    =self.data
        y      =self.model(x)
        y      =tf.keras.layers.Softmax()(y)
        y      =np.array([np.argmax(i) for i in y])
        figure=self.plotter  ('cm', [Y,y], [self.model.params[2]])
        image =self.converter(figure)
        with self.writer.as_default():
         tf.summary.image("Confusion Matrix", image, step=epoch)


class callback_custom_history(tf.keras.callbacks.Callback):
    
    def __init__(self, plotter, whdf):
        super().__init__()
        self.plotter=plotter
        self.whdf   =whdf
    
    def on_train_begin(self, logs=None):
        self.metric_key=[]
        self.metric_one=[]
        self.metric_two=[]
   
    def on_train_batch_end(self, batch, logs=None):
        keys=list(logs.keys())
        self.metric_one.append (logs[keys[0]])
        self.metric_two.append (logs[keys[1]])
        if batch==0:
         self.metric_key.append(     keys[0] )
         self.metric_key.append(     keys[1] )
    
    def on_train_end(self, logs=None):
        savedir=os.path.join(self.model.paths[4],'train')
        self.whdf(os.path.join(savedir,'metrics'+'.h5')   ,
                  [self.metric_key[0], self.metric_key[1]],
                  [self.metric_one   , self.metric_two   ] )
        fig=self.plotter('metrics'              ,
                         [[self.metric_key[0]],
                          [self.metric_one   ] ],
                         [['b'               ] ] )
        plt.savefig(os.path.join(savedir,self.metric_key[0]+'.png'),format='png')
        plt.clf()
        fig=self.plotter('metrics'              ,
                         [[self.metric_key[1]],
                          [self.metric_two   ] ],
                         [['b'               ] ] )
        plt.savefig(os.path.join(savedir,self.metric_key[1]+'.png'),format='png')


