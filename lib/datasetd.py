##################################################
### GRAPH LEARNING WITH TENSORFLOW 2           ###
### CONVOLUTIONAL GRAPH NEURAL NETWORKS (GNNs) ###
### DATASET FUNCTION DEFINITIONS               ###
### by: OAMEED NOAKOASTEEN                     ###
##################################################

def dataset_get_cora(PATH,VISUALIZE=False):
 def convert_paper_id_subject_to_indeces(PAPERS,CITATIONS):
  def get_dict(X):
   return {name:idx for idx, name in enumerate(X)}
  class_idx            =get_dict(sorted(PAPERS["subject" ].unique()))
  paper_idx            =get_dict(sorted(PAPERS["paper_id"].unique()))
  PAPERS   ["paper_id"]=PAPERS   ["paper_id"].apply(lambda name : paper_idx[name ])
  CITATIONS["source"  ]=CITATIONS["source"  ].apply(lambda name : paper_idx[name ])
  CITATIONS["target"  ]=CITATIONS["target"  ].apply(lambda name : paper_idx[name ])
  PAPERS   ["subject" ]=PAPERS   ["subject" ].apply(lambda value: class_idx[value])
 def visualize_cora(PAPERS,CITATIONS,SIZE,PATH):
  import                  networkx as nx
  from matplotlib  import pyplot   as plt
  filename  =os.path.join(PATH,'samples','samples'+'.png')
  colors    =PAPERS["subject"].tolist()
  cora_graph=nx.from_pandas_edgelist(CITATIONS.sample(n=SIZE))
  subjects  = list(PAPERS[PAPERS["paper_id"].isin(list(cora_graph.nodes))]["subject"])
  plt.figure(figsize=(10, 10))
  nx.draw_spring(cora_graph, node_size=15, node_color=subjects)
  plt.savefig(filename, format='png', transparent=True)
 import os
 import pandas     as pd
 import tensorflow as tf
 filename    ="cora.tgz"
 url         ="https://linqs-data.soe.ucsc.edu/public/lbc/cora.tgz"
 path_to_file=tf.keras.utils.get_file(fname  =filename                           ,
                                      origin =url                                ,
                                      extract=True                                )
 data_to_dir =os.path.join           (os.path.dirname(path_to_file)              ,
                                      "cora"                                      )
 citations   =pd.read_csv            (os.path.join   (data_to_dir,"cora.cites"  ),
                                      sep    ="\t"                               ,
                                      header =None                               ,
                                      names  =["target","source"]                 )
 papers      =pd.read_csv            (os.path.join   (data_to_dir,"cora.content"), 
                                      sep    ="\t"                               ,
                                      header =None                               , 
                                      names  =([ "paper_id"                         ]+ 
                                               [f"term_{idx}" for idx in range(1433)]+ 
                                               [ "subject"                          ] ))
 convert_paper_id_subject_to_indeces(papers,citations          )
 if VISUALIZE:
  visualize_cora                    (papers,citations,1500,PATH)
 return papers, citations


